/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo;

import com.copernic.demo.service.serviceConversor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ruben
 */
@Controller
@Slf4j
public class ControllerMain {
    
    @Autowired
    private serviceConversor conversor;
    
    
    @GetMapping("/conversor")
    public String conversor(){
        return "conversor";
    }
    
    @PostMapping("/conversor")
    public String convertir(@RequestParam(name = "milles",required = true) double milles,Model model){
        double kilometres = conversor.conversorMilles(milles);
        model.addAttribute("kilometres", kilometres);
        return "resultat";
        
    }
    
}
