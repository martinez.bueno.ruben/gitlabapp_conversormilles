/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

/**
 *
 * @author Ruben
 */
@Service
public class serviceConversor {
    
    public double conversorMilles(double milles){
        double kilometres = milles * 1.609;
        
        return kilometres;
    }
            
}
